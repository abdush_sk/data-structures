package main

import (
	"math"
)

// Binary Search Tree.
type Tree struct {
	Root *Node
}

type Node struct {
	Data  int
	Left  *Node
	Right *Node
}

func NewTree() *Tree {
	return &Tree{}
}

// Small goes left, big goes right.
func (t *Tree) Add(d int) {
	if t.Root == nil {
		t.Root = &Node{
			Data: d,
		}
	} else {
		t.Root.add(d)
	}
}

func (n *Node) add(d int) {
	if n.Data < d {
		if n.Right != nil {
			n.Right.add(d)
		} else {
			n.Right = &Node{
				Data: d,
			}
		}
	} else {
		if n.Left != nil {
			n.Left.add(d)
		} else {
			n.Left = &Node{
				Data: d,
			}
		}
	}
}

//Returns height of the tree
func (t *Tree) Height() int {
	if t.Root == nil {
		return 0
	}
	return t.Root.height()
}

func (n *Node) height() int {
	retLeft := 1
	retRight := 1
	if n.Left != nil {
		retLeft += n.Left.height()
	}
	if n.Right != nil {
		retRight += n.Right.height()
	}

	ret := math.Max(float64(retLeft), float64(retRight))
	return int(ret)
}

//Returns all the node data in the form of array
func (t *Tree) Flatten() []int {
	if t.Root == nil {
		return []int{}
	}
	return t.Root.flatten()
}
func (n *Node) flatten() []int {
	z := []int{n.Data}

	if n.Left != nil {
		z = append(z, n.Left.flatten()...)
	}
	if n.Right != nil {
		z = append(z, n.Right.flatten()...)
	}
	return z
}

//Returns number of nodes in an array
func (t *Tree) Count() int {
	if t.Root == nil {
		return 0
	}
	return t.Root.count()
}

func (n *Node) count() int {
	ret := 1
	if n.Left != nil {
		ret += n.Left.count()
	}
	if n.Right != nil {
		ret += n.Right.count()
	}
	return ret
}
