package main

import (
	"errors"
)

type Array struct {
	length int
	data   []int
}

//initializes array
func NewArray() *Array {
	e := Array{
		length: 0,
		data:   make([]int, 100),
	}
	return &e
}

//returns length of array
func (a *Array) Count() int {
	return a.length
}

//Add elements in array
func (a *Array) Add(x int) {
	if a.length == len(a.data) {
		a.resize(2 * len(a.data))
	}
	a.data[a.length] = x
	a.length++
}

//Set elements accord to index in array
func (a *Array) Set(i int, x int) error {
	if i < 0 || i >= a.length {
		return errors.New("Out of bound")
	}
	a.data[i] = x
	return nil
}

//Removes element according to the index passed
func (a *Array) Remove(i int) error {
	if i < 0 || i >= a.length {
		return errors.New("Out of Bound")
	}
	for j := i; j < a.length; j++ {
		a.data[j] = a.data[j+1]
	}
	a.length--
	return nil
}

//returns data according to the index passed
func (a *Array) Get(i int) (int, error) {
	if i >= a.length {
		return 0, errors.New("Out of Bound")
	}
	return a.data[i], nil
}

func (a *Array) resize(newCapacity int) {
	newArr := make([]int, newCapacity)
	for i, data := range a.data {
		newArr[i] = data
	}
	a.data = newArr
}
