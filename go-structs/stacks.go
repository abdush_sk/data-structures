package main

import "errors"

type Entry struct {
	data int    //integer stored
	next *Entry //Entry to the previous element
}

type Stack struct {
	top    *Entry //Pointer to the entry of the last added
	length int    //lenght of the stack
}

//Push integer to a stack
func (s *Stack) Push(x int) {
	e := Entry{
		data: x,
		next: s.top,
	}
	s.top = &e
	s.length++
}

//Count returns the length of the stack
func (s *Stack) Count() int {
	return s.length
}

//Top returns the top of the stack
func (s *Stack) Top() (int, error) {
	if s.top == nil {
		return 0, errors.New("Empty Stack")
	}
	return s.top.data, nil
}

//Pop last added interger of a stack
func (s *Stack) Pop() error {
	if s.length == 0 {
		return errors.New("Empty Stack")
	}
	if s.top == nil {
		s.top = &Entry{}
	}
	if s.top.next == nil {
		s.top.next = &Entry{}
	}
	// fmt.Println("--------------------------------------------")
	// fmt.Printf("old top data is %v \n", *s.top)
	// fmt.Printf("New top data will be %v \n", *s.top.next)
	// fmt.Println("--------------------------------------------")

	e := *s.top.next
	s.top = &e
	s.length--
	return nil
}
