package main

import (
	"errors"
	"fmt"
)

type LinkedList struct {
	front  *entry //Pointer to the first entry
	rear   *entry //Pointer to the last entry
	length int    //lenght of the stack
}

type entry struct {
	data     int    //integer stored
	next     *entry //Entry to the next element from ^
	previous *entry //Entry to the previous element from below
}

//PushRear: adds entry to the rear of the linkedList
func (l *LinkedList) PushRear(z int) {
	if l.length == 0 {
		e := &entry{
			data: z,
		}
		l.front = e
		l.rear = e
	} else {
		e := &entry{
			data:     z,
			previous: l.rear,
		}
		l.rear.next = e
		l.rear = e
	}
	l.length++
}

//PushFront: adds entry to the front of the linkedList
func (l *LinkedList) PushFront(z int) {
	if l.length == 0 {
		e := &entry{
			data: z,
		}
		l.front = e
		l.rear = e
	} else {
		e := &entry{
			data: z,
			next: l.front,
		}
		l.front.previous = e
		l.front = e
	}
	l.length++
}

//Count returns the length of the stack
func (l *LinkedList) Count() int {
	return l.length
}

//Front returns the front of the linkedList
func (l *LinkedList) Front() (int, error) {
	if l.length == 0 {
		return 0, errors.New("length found 0")
	}
	if l.front == nil {
		return 0, errors.New("rear found nil")
	}
	return l.front.data, nil
}

//Rear returns the rear of the linkedList
func (l *LinkedList) Rear() (int, error) {
	if l.length == 0 {
		return 0, errors.New("length found 0")
	}
	if l.rear == nil {
		return 0, errors.New("rear found nil")
	}
	return l.rear.data, nil
}

//Pop Removes the first entry of the linkedList
func (l *LinkedList) PopFront() error {
	if l.length == 0 {
		return errors.New("Length found 0")
	}
	if l.front.next == nil && l.front.previous == nil {
		fmt.Println("entered here")
		l.front = nil
		l.rear = nil
		l.length--
		return nil
	}
	l.front = l.front.next
	l.front.previous = nil
	l.length--
	return nil
}

//PopRear Removes the last/rear entry of the linkedList
func (l *LinkedList) PopRear() error {
	if l.length == 0 {
		return errors.New("Length found 0")
	}
	if l.rear.next == nil && l.rear.previous == nil {
		fmt.Println("entered here")
		l.front = nil
		l.rear = nil
		l.length--
		return nil
	}
	l.rear = l.rear.previous
	l.rear.next = nil
	l.length--
	return nil
}

func (l *LinkedList) Data() []int {
	if l.length == 0 {
		return []int{0}
	}
	return l.front.dataHelper()
}
func (l *entry) dataHelper() []int {
	ret := []int{l.data}
	if l.next != nil {
		ret = append(ret, l.next.dataHelper()...)
	}
	return ret
}
