package main

import "fmt"

type graphList struct {
	edges   [][]int
	visited []bool
}

func NewGraphList(n int) *graphList {
	edges := make([][]int, n)

	return &graphList{
		edges:   edges,
		visited: make([]bool, n),
	}
}

func (g *graphList) AddEdge(s, e int) {
	g.edges[s] = append(g.edges[s], e)
	g.edges[e] = append(g.edges[e], s)
}

func (g *graphList) DFS(i int) []int {
	g.visited = make([]bool, len(g.edges))
	return g._dfs(i)
}

func (g *graphList) _dfs(i int) []int {
	ret := []int{}
	if g.visited[i] {
		return ret
	}

	g.visited[i] = true
	ret = append(ret, i)

	for _, v := range g.edges[i] {
		ret = append(ret, g._dfs(v)...)
	}
	return ret
}

func mainG() {
	g := NewGraphList(10)
	g.AddEdge(0, 1)
	g.AddEdge(1, 2)
	g.AddEdge(2, 3)
	g.AddEdge(0, 8)
	g.AddEdge(0, 9)

	fmt.Println(g.DFS(0))
	// fmt.Println(reachableNodes([][]int{
	// 	{1, 2, 5},
	// 	{0, 3, 3},
	// 	{1, 3, 2},
	// 	{2, 3, 4},
	// 	{0, 4, 1},
	// }, 7, 5))
	fmt.Println("hell0")

}
