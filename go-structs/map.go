package main

import "fmt"

type Map struct {
	capacity int
	data     []LinkedList
}

func NewMap() *Map {
	return &Map{
		data:     make([]LinkedList, 20),
		capacity: 20,
	}
}

func small(z int, threshold int) int {
	return z % threshold
}

func (m *Map) Add(z int) {
	position := small(z, m.capacity)
	fmt.Printf("added in %v \n", position)
	m.data[position].PushFront(z)
}
func (m *Map) Get(pos int) []int {
	return m.data[pos].Data()
}

func mainMap() {
	testObj := NewMap()
	testObj.Add(10)
	testObj.Add(110)
	testObj.Add(150)
	testObj.Add(11)
	fmt.Println(testObj.Get(10))
}
