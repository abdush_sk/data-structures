package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLinkedListBasic(t *testing.T) {
	//declaring
	testObj := LinkedList{}

	testObj.PushRear(10)
	testObj.PushFront(20)
	testObj.PushFront(40)
	testObj.PopRear()
	testObj.PopFront()

	z, e := testObj.Front()
	assert.Nil(t, e)
	assert.Equal(t, z, 20)

	z, e = testObj.Rear()
	assert.Nil(t, e)
	assert.Equal(t, z, 20)

	assert.Equal(t, testObj.Count(), 1)
}

func TestLinkedListCornerCases(t *testing.T) {
	//declaring
	testObj := LinkedList{}

	//testing on empty linked list
	err := testObj.PopFront()
	assert.Nil(t, err)

	err = testObj.PopRear()
	assert.Nil(t, err)

	data, err := testObj.Front()
	assert.Nil(t, err)
	assert.Equal(t, data, 0)

	data, err = testObj.Rear()
	assert.Nil(t, err)
	assert.Equal(t, data, 0)

	assert.Equal(t, testObj.Count(), 0)

	//testing after pushing front in a linked list
	testObj.PushFront(13)

	data, err = testObj.Front()
	assert.Nil(t, err)
	assert.Equal(t, data, 13)

	data, err = testObj.Rear()
	assert.Nil(t, err)
	assert.Equal(t, data, 13)

	assert.Equal(t, testObj.Count(), 1)

	//testing after pushing rear in a linked list

	testObj.PushRear(56)

	data, err = testObj.Front()
	assert.Nil(t, err)
	assert.Equal(t, data, 13)

	data, err = testObj.Rear()
	assert.Nil(t, err)
	assert.Equal(t, data, 56)

	assert.Equal(t, testObj.Count(), 2)
}

func TestLinkedListData(t *testing.T) {
	//declaring
	testObj := LinkedList{}

	for i := 0; i < 5; i++ {
		testObj.PushFront(i * 10)
	}
	for i := 5; i < 10; i++ {
		testObj.PushRear(i * 10)
	}
	assert.Equal(t, testObj.Data(), []int{40, 30, 20, 10, 0, 50, 60, 70, 80, 90})

	testObj.PopRear()
	testObj.PopFront()

	assert.Equal(t, testObj.Data(), []int{30, 20, 10, 0, 50, 60, 70, 80})

}
