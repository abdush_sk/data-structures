package main

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Unit Test.
func TestStackBasic(t *testing.T) {
	s := Stack{}
	assert.Equal(t, 0, s.Count())
	s.Push(10)
	assert.Equal(t, 1, s.Count())
	data, err := s.Top()
	assert.Nil(t, err)
	assert.Equal(t, 10, data)

	s.Pop()
	assert.Equal(t, 0, s.Count())
}

// Corner Cases.
func TestStackCornerCases(t *testing.T) {
	s := Stack{}
	assert.Equal(t, 0, s.Count())
	assert.Nil(t, s.Pop())
	assert.Equal(t, 0, s.Count())

	s = Stack{}
	_, err := s.Top()
	assert.Nil(t, err)

	s.Push(math.MaxInt32)
	data, _ := s.Top()
	assert.Equal(t, math.MaxInt32, data)
}

// Stress Test.
func TestStackStress(t *testing.T) {
	s := Stack{}
	for i := 0; i < 1000000; i++ {
		s.Push(i)
	}
}
