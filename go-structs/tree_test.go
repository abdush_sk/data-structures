package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTreeBasic(t *testing.T) {
	testObj := NewTree()

	//root
	testObj.Add(20)

	//adding left of root
	testObj.Add(10)
	testObj.Add(5)
	testObj.Add(15)
	testObj.Add(17)

	//adding right of root
	testObj.Add(30)
	testObj.Add(35)
	testObj.Add(25)

	assert.Equal(t, testObj.Count(), 8)
	assert.Equal(t, testObj.Height(), 4)
	assert.Equal(t, testObj.Flatten(), []int{20, 10, 5, 15, 17, 30, 25, 35})
}

func TestTreeCornerCases(t *testing.T) {
	testObj := NewTree()

	assert.Equal(t, testObj.Count(), 0)
	assert.Equal(t, testObj.Height(), 0)
	assert.Equal(t, testObj.Flatten(), []int{})

	testObj.Add(20)
	assert.Equal(t, testObj.Count(), 1)
	assert.Equal(t, testObj.Height(), 1)
	assert.Equal(t, testObj.Flatten(), []int{20})

}
