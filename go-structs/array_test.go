package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArrayBasic(t *testing.T) {

	//initializing array
	testArray := NewArray()

	assert.Equal(t, testArray.Count(), 0)

	testArray.Add(52)

	assert.Equal(t, testArray.Count(), 1)

	data, err := testArray.Get(0)
	assert.Nil(t, err)
	assert.Equal(t, data, 52)

	//adding more than capacity
	for i := 0; i < 150; i++ {
		testArray.Add(i)
	}
	assert.Equal(t, testArray.Count(), 151)

}

func TestArrayCornerCases(t *testing.T) {
	//initializing array
	testArray := NewArray()

	assert.Equal(t, testArray.Count(), 0)

	//removing from an empty array
	err := testArray.Remove(0)
	assert.Nil(t, err)

	//Setting
	err = testArray.Set(10, 100)
	assert.Nil(t, err)
	assert.Equal(t, testArray.Count(), 0)

	data, err := testArray.Get(0)
	assert.Nil(t, err)
	assert.Equal(t, data, 0)

}
